import { personsImgs } from "../utils/images";
import { RiDashboardLine, RiGiftLine, RiCustomerService2Fill, RiMessage2Line, RiSettings2Fill } from 'react-icons/ri';
import { MdOutlineInventory } from 'react-icons/md';

// Define the type for your navigation link data.
type NavigationLink = {
  id: number;
  title: string;
  icon: JSX.Element;
};


// Navigation links data
export const navigationLinks: NavigationLink[] = [
  { id: 1, title: "Home", icon: <RiDashboardLine /> },
  { id: 2, title: "Orders", icon: <RiGiftLine /> },
  { id: 3, title: "Customers", icon: <RiCustomerService2Fill /> },
  { id: 4, title: "Inventory", icon: <MdOutlineInventory /> },
  { id: 5, title: "Conversations", icon: <RiMessage2Line /> },
  { id: 6, title: "Settings", icon: <RiSettings2Fill /> },
];

export const transactions = [
  {
    id: 11,
    name: "Sarah Parker",
    image: personsImgs.person_four,
    date: "23/12/04",
    amount: 22000,
  },
  {
    id: 12,
    name: "Krisitine Carter",
    image: personsImgs.person_three,
    date: "23/07/21",
    amount: 20000,
  },
  {
    id: 13,
    name: "Irene Doe",
    image: personsImgs.person_two,
    date: "23/08/25",
    amount: 30000,
  },
];

export const reportData = [
  {
    id: 14,
    month: "Jan",
    value1: 45,
    value2: null,
  },
  {
    id: 15,
    month: "Feb",
    value1: 45,
    value2: 60,
  },
  {
    id: 16,
    month: "Mar",
    value1: 45,
    value2: null,
  },
  {
    id: 17,
    month: "Apr",
    value1: 45,
    value2: null,
  },
  {
    id: 18,
    month: "May",
    value1: 45,
    value2: null,
  },
];

export const budget = [
  {
    id: 19,
    title: "Iphone 14",
    date: "12 sept 2022",
    amount: "N730,000.00 x 1",
    history:"pending"
  },
  {
    id: 20,
    title: "Iphone 14",
    date: "12 sept 2022",
    amount: "N730,000.00 x 1",
    history:"pending"

  },
  {
    id: 21,
    title: "Iphone 14",
    date: "12 sept 2022",
    amount: "N730,000.00 x 1",
    history:"pending"

  },
  {
    id: 22,
    title: "Iphone 14",
    date: "12 sept 2022",
    amount: "N730,000.00 x 1",
    history:"completed"

  },
  {
    id: 23,
    title: "Iphone 14",
    date: "12 sept 2022",
    amount: "N730,000.00 x 1",
    history:"pending"

  },
  {
    id: 23,
    title: "Iphone 14",
    date:"12 sept 2022",
    amount: "N730,000.00 x 1",
    history:"completed"

  },
  {
    id: 23,
    title: "Iphone 14",
    date: "12 sept 2022",
    amount: "N730,000.00 x 1",
    history:"completed"

  },
  {
    id: 23,
    title: "Iphone 14",
    date: "12 sept 2022",
    amount: "N730,000.00 x 1",
    history:"pending"

  },
];

export const subscriptions = [
  {
    id: 24,
    title: "LinkedIn",
    due_date: "23/12/04",
    amount: 20000,
  },
  {
    id: 25,
    title: "Netflix",
    due_date: "23/12/10",
    amount: 5000,
  },
  {
    id: 26,
    title: "DSTV",
    due_date: "23/12/22",
    amount: 2000,
  },
];

export const savings = [
  {
    id: 27,
    image: personsImgs.person_one,
    saving_amount: 250000,
    title: "Pay kid bro’s fees",
    date_taken: "23/12/22",
    amount_left: 40000,
  },
];
