import Content from "./Content/Content";
import Sidebar from "./Sidebar/SideBar";

const Main = () => {
  return (
    <div className="app">
      <Sidebar />
      <Content />
    </div>
  );
};

export default Main;
