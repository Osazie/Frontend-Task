import { ContentTop } from "../../components";
import "./Content.css";
import { Outlet } from "react-router-dom";

const Content = () => {
  return (
    <div className="main-content">
      <ContentTop />
      <div>
        <Outlet />
      </div>{" "}
    </div>
  );
};

export default Content;
