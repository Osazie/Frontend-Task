import { useEffect, useState } from "react";
import { navigationLinks } from "../../data/data";
import "./Sidebar.css";
import { useContext } from "react";
import { SidebarContext } from "../../context/sidebarContext";
import {RiCustomerService2Fill} from "react-icons/ri"
import { SiBaremetrics } from "react-icons/si";
import {BiLogOut} from "react-icons/bi"

const Sidebar = () => {
  const [activeLinkIdx] = useState(1);
  const [sidebarClass, setSidebarClass] = useState("");
  const { isSidebarOpen } = useContext(SidebarContext);

  useEffect(() => {
    if (isSidebarOpen) {
      setSidebarClass("sidebar-change");
    } else {
      setSidebarClass("");
    }
  }, [isSidebarOpen]);

  return (
    <div className={`sidebar ${sidebarClass}`}>
      <div className="user-info">
        <div className="icon">
          {" "}
          <SiBaremetrics />
        </div>
        <span className="info-name">Metrix</span>
      </div>

      <nav className="navigation">
        <ul className="nav-list">
          {navigationLinks.map((navigationLink) => (
            <li className="nav-item" key={navigationLink.id}>
              <a
                href="#"
                className={`nav-link ${
                  navigationLink.id === activeLinkIdx ? "active" : null
                }`}
              >
                <div className="nav-link-icon">{navigationLink.icon}</div>
                <span className="nav-link-text">{navigationLink.title}</span>
              </a>
            </li>
          ))}
        </ul>

        {/* Logout Menu */}
      

      <div className="logout">
      <div className="support-buttons">
          <button className="contants"><span><RiCustomerService2Fill/></span>Contact Support</button>
          <button className="gifts">
            Free gift awaits<span>upgrade your account</span>
          </button>
        </div>

        <div className="logout-menu">
          <span><BiLogOut/></span>
          Logout
        </div>
      </div>
      </nav>
    </div>
  );
};

export default Sidebar;
