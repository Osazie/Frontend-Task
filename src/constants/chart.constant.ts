import { theme } from 'twin.macro';

// Define the custom type for the theme
interface CustomTheme {
  colors: {
    [key: string]: {
      [key: string]: string;
    };
    // Add more properties if your theme has other nested properties
  };
  // Add more properties if your theme has top-level properties
}

// Access the 'colors' property of the theme
const twColor: CustomTheme['colors'] = theme`colors`;

export const COLOR_1: string = twColor.indigo['600'];
export const COLOR_2: string = twColor.blue['500'];
export const COLOR_3: string = twColor.emerald['500'];
export const COLOR_4: string = twColor.amber['500'];
export const COLOR_5: string = twColor.red['500'];
export const COLOR_6: string = twColor.purple['500'];
export const COLOR_7: string = twColor.cyan['500'];

export const COLOR_1_LIGHT: string = twColor.indigo['100'];
export const COLOR_2_LIGHT: string = twColor.blue['100'];
export const COLOR_3_LIGHT: string = twColor.emerald['100'];
export const COLOR_4_LIGHT: string = twColor.amber['100'];
export const COLOR_5_LIGHT: string = twColor.red['100'];
export const COLOR_6_LIGHT: string = twColor.purple['100'];
export const COLOR_7_LIGHT: string = twColor.cyan['100'];

export const COLORS: string[] = [
  COLOR_1,
  COLOR_2,
  COLOR_3,
  COLOR_4,
  COLOR_5,
  COLOR_6,
  COLOR_7,
];

export const COLORS_LIGHT: string[] = [
  COLOR_1_LIGHT,
  COLOR_2_LIGHT,
  COLOR_3_LIGHT,
  COLOR_4_LIGHT,
  COLOR_5_LIGHT,
  COLOR_6_LIGHT,
  COLOR_7_LIGHT,
];
