import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Main from "../layout/main";
import Conversation from "../pages/Conversation";
import DashboardOverView from "../pages/DashboardOverView";


const AllRoutes = () => {
  const Index = createBrowserRouter([
    {
      path: "/",
      children: [
        {
          element: <Main />,
          children: [
            {
              path: "/",
              children: [
                {
                  index: true,
                  element: <DashboardOverView />,
                },
                {
                  path:"/conversation" ,
                  element: <Conversation />,
                },
              ],
            },
          ],
        },
      ],
    },
  ]);
  return <RouterProvider router={Index}></RouterProvider>;
};

export default AllRoutes;
