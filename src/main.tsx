import ReactDOM from "react-dom/client";
import "./App.css";
import AllRoutes from "./routes/AllRoutes.tsx";
import { SidebarProvider } from "./context/sidebarContext.tsx";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <SidebarProvider>
    <AllRoutes />
  </SidebarProvider>
);
