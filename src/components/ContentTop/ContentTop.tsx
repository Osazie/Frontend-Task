import "./ContentTop.css";
import { useContext } from "react";
import { SidebarContext } from "../../context/sidebarContext";
import { IoIosArrowDown } from "react-icons/io";
import { FiMenu } from "react-icons/fi";
import { BsBellFill } from "react-icons/bs";
import { personsImgs } from "../../utils/images";

const ContentTop = () => {
  const { toggleSidebar } = useContext(SidebarContext);

  return (
    <div className="main-content-top">
      <div className="content-top-left">
        <button
          type="button"
          className="sidebar-toggler"
          onClick={() => toggleSidebar()}
        >
          <FiMenu size={30} />
        </button>
        <h3 className="content-top-title">DashBoard</h3>
      </div>
      <div className="content-top-btns">
        <button type="button" className="nanny-btn">
          nanny shop{" "}
          <span>
            <IoIosArrowDown />
          </span>
        </button>

        <button className="notification-btn content-top-btn">
          <div className="bell">
            <BsBellFill />
          </div>
          <span className="notification-btn-dot"></span>
        </button>

        <button type="button" className="img-btn">
          <img src={personsImgs.person_two} className="img" alt="profile image" />
        </button>
      </div>
    </div>
  );
};

export default ContentTop;
