import  { useState } from "react";
import "./PieChart.css";
import { IoIosArrowDown } from "react-icons/io";
import ReactApexChart from "react-apexcharts";

const PieChart = () => {
  const [seriesData] = useState([44, 55, 41, 17, 15]);

  const chartType: "area" | "line" | "donut" | "bar" | "pie" = "donut";


  const options = {
    chart: {
      type: chartType, 
    },
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 200,
          },
          legend: {
            position: "bottom",
          },
        },
      },
    ],
  };

  return (
    <div className="container">
      <div className="test">
        <p>Marketing</p>
        <div className="lg-value">
          this week{" "}
          <span>
            <IoIosArrowDown />
          </span>
        </div>
      </div>
      <div className="newtext">
        <span>Acquisition</span>
        <span>Purchase</span>
        <span>Retention</span>
      </div>
      <div className="holder">
        <div id="chart">
          <ReactApexChart options={options} series={seriesData} type="donut" />
        </div>
      </div>
    </div>
  );
};

export default PieChart;
