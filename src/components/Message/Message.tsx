import React, { useState } from "react";
import "./Message.css";
import { RiSendPlaneFill } from "react-icons/ri";
import { BsPlus, BsEmojiSmile } from "react-icons/bs";
import { TiTick } from "react-icons/ti";
import { BiSolidCircle } from "react-icons/bi";
import { personsImgs } from "../../utils/images";

interface Message {
  text: string;
  sender: "user" | "other";
}

const Message: React.FC = () => {
  const [messages, setMessages] = useState<Message[]>([]);
  const [newMessage, setNewMessage] = useState<string>("");

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setNewMessage(e.target.value);
  };

  const handleSendMessage = () => {
    if (newMessage.trim() !== "") {
      setMessages([...messages, { text: newMessage, sender: "user" }]);
      setNewMessage("");
    }
  };

  return (
    <div className="chatbox">
      <div className="top">
        <div className="profile">
          <div className="Img">
            <img src={personsImgs.person_one} />
          </div>
          <div className="test">
            <p>jane doe</p>
            <div className="online-div">
              <span>
                <BiSolidCircle
                  style={{
                    color: "#5570f1",
                    fontSize: "10px",
                    marginRight: "10px",
                  }}
                />
              </span>
              <p>Online 12:55am</p>
            </div>
          </div>
          <div className="test2"></div>
        </div>

        <div className="second-div">
          <div className="second-div-holder">
            <div style={{color:"#000", height:"20px", width: "100px", display:"flex", justifyContent:"center", alignItems:"center", borderRadius:"8px", background:"#ffead1", fontSize:"10px", fontWeight:"bold"}}>new customer</div>
            <div style={{color:"#5570f1", height:"20px", width: "100px", display:"flex", justifyContent:"center", alignItems:"center", borderRadius:"8px",  fontSize:"10px", fontWeight:"bold"}}>View Orders</div>
          </div>
          <div className="orders">Orders</div>
        </div>
      </div>
      <div className="chatbox-messages">
        <div className="chatbox">
          <div className="message other">
            hello, i want to make enquiries about your product{" "}
          </div>
          <p className="p-n">12:25am</p>

          <div className="message user">
            hello janet, thank you for reaching out{" "}
          </div>
          <p className="p-tag">
            12:25am
            <span>
              <TiTick />
            </span>
          </p>

          <div className="message user">what do you need to know </div>
          <p className="p-tag">
            12:25am
            <span>
              <TiTick />
            </span>
          </p>

          <div className="message other">
            i want to know if the price is negotiable, i need to unit{" "}
          </div>
          <p>12:25am</p>
        </div>
      </div>
      <div className="chatbox-input">
        <div className="input-holder">
          <div className="logo">
            <BsPlus style={{ color: "#000", fontSize: "20px" }} />
          </div>{" "}
          <input
            type="text"
            placeholder="your message"
            value={newMessage}
            onChange={handleInputChange}
          />
        </div>
        <div className="logo2">
          <BsEmojiSmile style={{ color: "#000", fontSize: "20px" }} />
        </div>{" "}
        <button onClick={handleSendMessage}>
          Send <RiSendPlaneFill />
        </button>
      </div>
    </div>
  );
};

export default Message;
