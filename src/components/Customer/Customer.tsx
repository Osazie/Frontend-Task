import "./Customer.css";
import { personsImgs } from "../../utils/images";
import { HiOutlineSearch } from "react-icons/hi";

const transactions = [
  {
    id: 11,
    name: "Jane Done",
    image: personsImgs.person_four,
    time: "12:25am",
    type: "new",

    message: "Hi, i want make enquries about you..",
  },
  {
    id: 12,
    name: "Janet Adebayo",
    image: personsImgs.person_three,
    time: "12:25am",
    type: "new",
    message: "Hi, i want make enquries about you..",
  },
  {
    id: 13,
    name: "Kunle Adekunle",
    image: personsImgs.person_two,
    time: "12:25am",
    type: "new",
    message: "Hi, i want make enquries about you..",
  },
  {
    id: 11,
    name: "Jane Done",
    image: personsImgs.person_four,
    time: "12:25am",
    type: "new",

    message: "Hi, i want make enquries about you..",
  },
  {
    id: 12,
    name: "Janet Adebayo",
    image: personsImgs.person_three,
    time: "12:25am",
    type: "new",
    message: "Hi, i want make enquries about you..",
  },
  {
    id: 13,
    name: "Kunle Adekunle",
    image: personsImgs.person_two,
    time: "12:25am",
    type: "new",
    message: "Hi, i want make enquries about you..",
  },
];

const Customer = () => {
  return (
    <div className="grid-two-item grid-common grid-c4">
      <div className="grid-c-title">
        <h3 className="grid-c-title-text">Contacts</h3>
        <button className="grid-c-title-icon">34 </button>
      </div>
      <div className="grid-c-top text-silver-v1">
        <HiOutlineSearch />
        <input />
      </div>
      <div className="grid-c4-content bg-jet">
        <div className="grid-items">
          {transactions.map((budget) => (
            <div className="grid-item" key={budget.id}>
              <div className="grid-item-l">
                <div className="icon">
                  <img src={personsImgs.person_four} />
                </div>
                <p className="text text-silver-v1">
                  {budget.name} <span>{budget.message}</span>
                </p>
              </div>
              <div className="grid-item-r">
                <span className="text-2xl text-yellow-950">{budget.type}</span>

                <p className="text-silver-v1"> {budget.time}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Customer;
