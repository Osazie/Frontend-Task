import { useState } from "react";
import { iconsImgs } from "../../utils/images";
import "./Subscriptions.css";
import ReactApexChart from "react-apexcharts";

const Subscriptions = () => {
  const chartType: "area" | "line" | "donut" | "bar" | "pie" = "bar";

  const [chartData] = useState({
    series: [
      {
        name: "Net Profit",
        data: [44, 55, 57, 56, 61, 58, 63, 60, 66],
      },
      {
        name: "Revenue",
        data: [76, 85, 101, 98, 87, 105, 91, 114, 94],
      },
      {
        name: "Free Cash Flow",
        data: [35, 41, 36, 26, 45, 48, 52, 53, 41],
      },
    ],
    options: {
      chart: {
        type: chartType,
        height: 350,
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: "55%",
          endingShape: "rounded",
        },
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        show: true,
        width: 2,
        colors: ["transparent"],
      },
      xaxis: {
        categories: [
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
        ],
      },
      yaxis: {
        title: {
          text: "$ (thousands)",
        },
      },
      fill: {
        opacity: 1,
      },
      tooltip: {
        y: {
          formatter: (val: unknown) => `$ ${val} thousands`,
        },
      },
    },
  });

  return (
    <div className="subgrid-one-item grid-common grid-c5">
      <div className="grid-c-title">
        <h3 className="grid-c-title-text">Subscriptions</h3>
        <button className="grid-c-title-icon">
          <img src={iconsImgs.plus} />
        </button>
      </div>

      <div className="grid-c5-content">
        <div id="chart">
          <ReactApexChart
            options={chartData.options}
            series={chartData.series}
            type="bar"
            height={350}
          />
        </div>
        {/* <div className="chart-container">
        <div className="bar" style={{ height: "100%", width: "30px" }}></div>
        <div className="bar" style={{ height: "90%", width: "30px" }}></div>
          <div className="bar" style={{ height: "80%",width: "30px" }}></div>
          <div className="bar" style={{ height: "70%",width: "30px" }}></div>
          <div className="bar" style={{ height: "60%",width: "30px" }}></div>

          <div className="bar" style={{ height: "50%",width: "30px" }}></div>
          <div className="bar" style={{ height: "70%" ,width: "30px"}}></div>
          <div className="bar" style={{ height: "30%" ,width: "30px"}}></div>
        </div> */}
      </div>
    </div>
  );
};

export default Subscriptions;
