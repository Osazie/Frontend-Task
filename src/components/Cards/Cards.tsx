import { ReactNode } from "react";
import "./Cards.css";
import { IoIosArrowDown } from "react-icons/io";

interface Test {
  icon?: ReactNode;
  titletext?: string;
  value?: string;
  num?: string;
  num1?: string;
  large?: string;
  large1?: string;
  extra?: string;
  extra1?: string;
  extra2?: string;
  backgroundColor?: string;
}

const Cards = ({
  icon,
  titletext,
  value,
  num,
  num1,
  large,
  large1,
  extra,
  extra1,
  extra2,
  backgroundColor,
}: Test) => {
  return (
    <div className="grid-one-item grid-common grid-c1">
      <div className="grid-c1-content">
        <div className="Logo-holder">
          <p className="item" style={{ backgroundColor }}>
            {icon}
            {/* <SiBaremetrics /> */}
          </p>
          <div className="lg-value">
            this week{" "}
            <span>
              <IoIosArrowDown />
            </span>
          </div>
        </div>
        {/* <div className="card-wrapper">
          <span className="card-pin-hidden">**** **** **** </span>
          <span>1234</span>
        </div> */}
        <div className="card-logo-wrapper">
          <div>
            <p className="text-red-600">{titletext}</p>
            <p className="">
              {large} <span>{large1}</span>
            </p>{" "}
          </div>
          <div>
            <p className="">{extra}</p>
            <p className="">
              {extra1} <span>{extra2}</span>
            </p>{" "}
          </div>
          <div>
            <p className="">{value}</p>
            <p className="">
              {num} <span>{num1}</span>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Cards;
