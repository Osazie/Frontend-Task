
import { Customer, Message } from "../components";
import "./ContentMain.css";

const Conversation = () => {
  return (
    <div className="coversation">
      <div className="top-conversation">
        <div className="text2">conversation with customer</div>
        <button className="btn">new message</button>
      </div>
      <div className="coversation-holder">
        <div>
          <Customer/>
        </div>
        <div className="Message">
          <Message />
        </div>
      </div>
    </div>
  );
};

export default Conversation;
