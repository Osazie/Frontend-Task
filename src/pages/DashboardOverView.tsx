import "./ContentMain.css";
import { SiBaremetrics } from "react-icons/si";
import { SlPeople } from "react-icons/sl";
import { PiGift } from "react-icons/pi";
import Cards from "../components/Cards/Cards";
import { Budget, PieChart, Subscriptions } from "../components";

const DashboardOverView = () => {
  return (
    <div className="main-content-holder">
      <div className="content-grid-one">
        <Cards
          icon={<SiBaremetrics />}
          titletext="Sales"
          value="volume"
          large="N4,000,000,00"
          num="450"
          num1="+20.00%"
          backgroundColor="#EBEEFD"
        />
        <Cards
          icon={<SlPeople />}
          titletext="customers"
          value="Active"
          large="1250"
          large1="+15.80%"
          num="1180"
          num1="45%"
          backgroundColor="#FFF7ED"
        />
        <Cards
          icon={<PiGift />}
          titletext="All orders"
          value="Complete"
          large="450"
          num="445"
          extra="pending"
          extra1="15"
          backgroundColor="#FFF7ED"
        />
      </div>
      <div className="content-grid-two">
        <div className="grid-two-item">
          <div className="subgrid-two">
            <div className="grid-holder">
              <PieChart />
              <div className="grid-two-item">
                <div className="subgrid-two">
                  <Cards
                    icon={<PiGift />}
                    titletext="All orders"
                    value="Complete"
                    large="450"
                    num="445"
                    extra="pending"
                    extra1="15"
                    backgroundColor="#FFF7ED"
                  />
                  <Cards
                    icon={<PiGift />}
                    titletext="All orders"
                    value="Complete"
                    large="450"
                    num="445"
                    extra="pending"
                    extra1="15"
                    backgroundColor="#FFF7ED"
                  />
                </div>
              </div>
            </div>

            <div>
              <Subscriptions />
            </div>
          </div>
        </div>

        <Budget />
      </div>
    </div>
  );
};

export default DashboardOverView;
